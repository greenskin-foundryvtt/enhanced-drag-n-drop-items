import { initItemSheetDropHandler } from "./item-sheet";

Hooks.once("init", () => {
  initItemSheetDropHandler();
});
